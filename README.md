# Exploratory Data Analysis with Python and Jupyter Notebooks

## Overview

This repository has one Jupyter Notebook (Jupyter_EDA.ipynb) that outlines how to conduct some basic exploratory data analysis on the CSV file contained in this repository (world_population.csv). There is also a requirements.txt file that outlines all of the Python libraries needed for the Jupyter Notebook to function properly.

There is a blog article that goes along with this repository, which can be found [here](https://www.515tech.com/post/eda-with-python-and-jupyter-notebooks).
